from flask import Flask, render_template, request, redirect
from flask_sqlalchemy import SQLAlchemy  # add
from datetime import datetime  # add
from gsearch.googlesearch import search
import pdb
import duckduckgo
import httplib2
import wikipediaapi
import asyncio
import aiohttp
import json
import os
from flask_socketio import SocketIO, emit

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = ''  # add
db = SQLAlchemy(app)  # add
#socketio = SocketIO(app)


                     # Decorator to catch an event called "my event":
def status_completed(message):
    socketio.emit('my response', {'data': 'got it!'})

@app.route('/duckduck_go_search')
def get_duck_duck_go_results():
    try:
        search_string=request.args.get('query')
        uri = "https://api.duckduckgo.com/?q={search_string}&format=json&pretty=1&no_html=1&skip_disambig=1".format(search_string=search_string)
        h = httplib2.Http(".cache")
        resp, content = h.request(uri, "GET")
        content_json = json.loads(content)
        for topic in content_json["RelatedTopics"]:
            new_stuff = SearchDetailsProd(status="sucess",link=topic["FirstURL"],engine="duckduckgo",link_text=topic["Text"],name=search_string)
            db.session.add(new_stuff)
            db.session.commit()
        return {"status":"true"}         
    except:
        return {"status":"false"} 

@app.route('/wikepedia_search')
def get_wikipedia_page():
    search_string=request.args.get('query')
    wiki_wiki = wikipediaapi.Wikipedia('en')
    page_py = wiki_wiki.page(search_string)
    try:
        if page_py.exists(): 
            new_stuff = SearchDetailsProd(status="sucess",link=page_py.fullurl,engine="wikipedia",link_text=page_py.title,name=search_string)
            db.session.add(new_stuff)
            db.session.commit()
            return {"status":"true"}
        else:
            return {"status":"false"}
    except:
        return {"status":"false"}

@app.route('/show')
def get_results():
    searches = SearchDetailsProd.query.order_by(SearchDetailsProd.created_at).all()
    return render_template('show.html', searches=searches)

@app.route('/google_search')
def get_google_results():
    try:
        search_string=request.args.get('query')
        key = os.environ['key']
        cx = os.environ['cx']
        uri = "https://www.googleapis.com/customsearch/v1?key={key}&cx={cx}&q={search_string}".format(search_string=search_string,key=key,cx=cx)
        h = httplib2.Http(".cache")
        resp, content = h.request(uri, "GET")
        content_json = json.loads(content)
        for item in content_json['items']:
            new_stuff = SearchDetailsProd(status="sucess",link=item["link"],engine="google",link_text=item["title"],name=search_string)
            db.session.add(new_stuff)
            db.session.commit()
        return {"status":"true"}    
    except:
        return {"status":"false"}

@app.route('/search', methods=['GET','POST'])
def search():
    if request.method == 'POST':
        search = request.form['query']
        #try:
        return redirect('/?query={}'.format(search))
        #except:
            #return "There was a problem adding new stuff."
    else:
        return render_template('search.html' )

@app.route('/', methods=['GET', 'POST'])
def index():
    #pdb.set_trace()
    if request.method == 'POST':
        search = request.form['query']
        #status = request.form['status']
        #link = request.form['link']
        #engine = request.form['engine']
        #new_stuff = SearchDetails(name=name,status=status,link=link,engine=engine,)

        try:
            #db.session.add(new_stuff)
            #db.session.commit()
            return redirect('/?search={}'.format(search))
        except:
            return "There was a problem adding new stuff."

    else:
        #results = search('Full Stack Developer', num_results=5)
            
        tasks =  []
        loop = asyncio.new_event_loop()
        search_string =   request.args.get('query')
        tasks.append(loop.create_task(get_duck_duck_go_results(search_string)))
        tasks.append(loop.create_task(get_google_results(search_string)))
        tasks.append(loop.create_task(get_wikipedia_page(search_string)))
        try:
            loop.run_until_complete(asyncio.wait(tasks))
        finally:
            loop.close()
            status_completed("completed")

        searches = SearchDetails.query.order_by(SearchDetails.created_at).all()
        return render_template('index.html', searches=searches )

'''
def index():
    if request.method == 'POST':
        name = request.form['name']
        new_stuff = Grocery(name=name)

        try:
            db.session.add(new_stuff)
            db.session.commit()
            return redirect('/')
        except:
            return "There was a problem adding new stuff."

    else:
        groceries = Grocery.query.order_by(Grocery.created_at).all()
        return render_template('index.html', groceries=groceries)
'''
# add
# 
class SearchDetailsProd(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    status = db.Column(db.String(80), nullable=False)
    link = db.Column(db.String(2280), nullable=False)
    link_text = db.Column(db.String(2280), nullable=False)
    engine = db.Column(db.String(80), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)

    def __repr__(self):
        return '<SearchDetailsProd %r>' % self.name
# add
class Grocery(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False,
                           default=datetime.utcnow)

    def __repr__(self):
        return '<Grocery %r>' % self.name


if __name__ == '__main__':
    app.run(debug=True)
    #socketio.run(app)